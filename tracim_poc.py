import asyncio
import json

import aiohttp
import aiopubsub
import logwood
from aiohttp_sse_client import client as sse_client

BASE_URL = "http://localhost:8080"
EVENT_KEY= 'event'
AUTHOR_EVENT_KEY = "author"
SEARCHED_TOPICS = [
    ['event', 'user', 'modified'],
    # ['author', '2']
]
USER_ID = 1

auth=aiohttp.BasicAuth(
    login="admin@admin.admin",
    password="admin@admin.admin"
)
REACT_TO = (1,1)  # workspace id, content id.


client_session = aiohttp.ClientSession(auth=auth)
LIVE_MESSAGE_ENDPOINT="{base_url}/api/users/{user_id}/live_messages"
COMMENT_ENDPOINT = "{base_url}/api/workspaces/{workspace_id}/contents/{content_id}/comments"

async def comment(
        workspace_id:int,
        content_id: int,
        raw_content: str,
):
    url = COMMENT_ENDPOINT.format(
        base_url=BASE_URL,
        workspace_id=workspace_id,
        content_id=content_id,
    )
    result = await client_session.post(url, json={'raw_content': raw_content})
    json = await result.json()
    assert json
    assert result.status == 200

async def main():
    logwood.basic_config()
    hub = aiopubsub.Hub()
    event_publisher = aiopubsub.Publisher(hub, prefix=aiopubsub.Key(EVENT_KEY))
    author_event_publisher = aiopubsub.Publisher(hub, prefix=aiopubsub.Key(AUTHOR_EVENT_KEY))
    subscriber = aiopubsub.Subscriber(hub, 'user')
    for topic in SEARCHED_TOPICS:
        sub_key = aiopubsub.Key(*topic)
        subscriber.subscribe(sub_key)
        subscriber.add_async_listener(sub_key, reaction)
    async with sse_client.EventSource(
        LIVE_MESSAGE_ENDPOINT.format(
            base_url=BASE_URL,
            user_id=str(USER_ID)
        ), session=client_session
    ) as event_source:
        try:
            async for event in event_source:
                if event.type == 'message':
                    data = json.loads(event.data)
                    event_type = data.get('event_type')
                    splitted_event_type = event_type.split('.')
                    main_type = "default"
                    subtype = "default"
                    if len(splitted_event_type) >= 1:
                            main_type = splitted_event_type[0]
                    if len(splitted_event_type) >= 1:
                        subtype = splitted_event_type[1]
                    event_pub_key = aiopubsub.Key(main_type, subtype)
                    event_publisher.publish(event_pub_key, event)
                    fields = data.get("fields")
                    if fields:
                        author = fields.get('author')
                        if author:
                            user_id = author.get('user_id')
                            author_event_key = aiopubsub.Key(str(user_id))
                            author_event_publisher.publish(author_event_key, event)
                elif event.type == "stream-open":
                    print("opened connexion !")
        except ConnectionError:
            pass


async def reaction(key, message):
    print(key, message)
    # WARNING AVOID INFINITE TLM LOOP !!!
    try:
        public_name = json.loads(message.data).get("fields").get("user").get("public_name")
        raw_content = "J'ai modifié les données de l'utilisateur <strong>@{}</strong> !".format(public_name)
        await comment(workspace_id=REACT_TO[0], content_id=REACT_TO[1], raw_content=raw_content)
    except:
        print("erreur")


asyncio.get_event_loop().run_until_complete(main())