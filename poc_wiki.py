import asyncio
import json

import aiopubsub
import logwood
from aiohttp_sse_client import client as sse_client


BASE_KEY='wiki'
SEARCHED_TOPICS = [['wiki', 'enwiki']]
async def main():
    logwood.basic_config()
    hub = aiopubsub.Hub()
    publisher = aiopubsub.Publisher(hub, prefix=aiopubsub.Key(BASE_KEY))
    subscriber = aiopubsub.Subscriber(hub, 'user')
    for topic in SEARCHED_TOPICS:
        sub_key = aiopubsub.Key(*topic)
        subscriber.subscribe(sub_key)
        subscriber.add_async_listener(sub_key, print_message)

    async with sse_client.EventSource(
        'https://stream.wikimedia.org/v2/stream/recentchange'
    ) as event_source:
        try:
            async for event in event_source:
                data = json.loads(event.data)
                wiki = data.get('wiki')
                pub_key = aiopubsub.Key(wiki)
                publisher.publish(pub_key, event)
        except ConnectionError:
            pass


async def print_message(key, message):
    print(key, message)

asyncio.get_event_loop().run_until_complete(main())